<?php

/**
 * @file
 * Provides drush integration.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implements hook_drush_command().
 */
function code_coverage_drush_command() {
  $items = array();

  $items['code-coverage-process'] = array(
    'description' => dt('Process a coverage set of raw Xdebug data.'),
    'arguments' => array(
      'coverage set' => dt('Code coverage set ID.'),
    ),
    'aliases' => array('cc-process'),
  );
  $items['code-coverage-export'] = array(
    'description' => dt('Export a code coverage set as a json dump or to html files using --html.'),
    'arguments' => array(
      'coverage set' => dt('Code coverage set ID.'),
    ),
    'options' => array(
      'html' => 'Export line data combined with file contents as formatted HTML files.',
    ),
    'aliases' => array('cc-export'),
  );
  $items['code-coverage-summary'] = array(
    'description' => dt('Give a simple coverage summary of a processed test'),
    'arguments' => array(
      'coverage set' => dt('Code coverage set ID.'),
    ),
    'aliases' => array('cc-summary'),
  );
  $items['code-coverage-executable'] = array(
    'description' => dt('Determine the number of executable lines contained in a file. Files are loaded in order to determine lines of code so this should be used for files that are never executed.'),
    'arguments' => array(
      'path' => dt('Path to file.'),
    ),
    'options' => array(
      'full' => 'Export a full coverage entry formatted as json and including: path, executable, and data.',
    ),
    'aliases' => array('cc-executable'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Validate coverage set ID.
 *
 * @param $coverage_set
 *   A coverage set ID.
 *
 * @return
 *   TRUE if valid, otherwise FALSE.
 */
function drush_code_coverage_process_validate($coverage_set = NULL) {
  if (!$coverage_set) {
    return drush_set_error('COVERAGE_SET', dt('A coverage set ID is required.'));
  }
}

/**
 * Process a coverage set of raw Xdebug data.
 *
 * @param $coverage_set
 *   A coverage set ID.
 */
function drush_code_coverage_process($coverage_set) {
  module_load_include('process.inc', 'code_coverage');
  drush_print(code_coverage_process($coverage_set) ? dt('success') : dt('failure, results may have already been processed'));
}

/**
 * Validate coverage set ID.
 *
 * @param $coverage_set
 *   A coverage set ID.
 *
 * @return
 *   TRUE if valid, otherwise FALSE.
 */
function drush_code_coverage_export_validate($coverage_set = NULL) {
  if (!$coverage_set) {
    return drush_set_error('COVERAGE_SET', dt('A coverage set ID is required.'));
  }

  module_load_include('report.inc', 'code_coverage');
  if (!code_coverage_report_file_list($coverage_set)) {
    return drush_set_error('COVERAGE_SET', dt('Invalid coverage set ID.'));
  }
}

/**
 * Export a code coverage set to files.
 *
 * @param $coverage_set
 *   A coverage set ID.
 */
function drush_code_coverage_export($coverage_set) {
  // Attempt to create a directory to place exported data.
  $directory = 'temporary://code_coverage_' . $coverage_set;
  if (!file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
    return drush_set_error('TEMP_DIRECTORY', dt('Failed to create temporary directory.'));
  }

  if (drush_get_option('html')) {
    // Render the code coverage data for each file as HTML and export to a file.
    $files = code_coverage_report_file_list($coverage_set);
    foreach ($files as $file) {
      $page = code_coverage_report_view_file($coverage_set, $file->file_id);
      $lines = drupal_render(array_pop($page));
      file_unmanaged_save_data($lines, $directory . '/' . $file->file_id . '.html', FILE_EXISTS_REPLACE);
    }

    // Save summary data as JSON.
    file_unmanaged_save_data(json_encode($files), $directory . '/summary.json', FILE_EXISTS_REPLACE);
  }
  else {
    $files = db_select('code_coverage', 'c')
      ->fields('c', array('path', 'executed', 'executable', 'data'))
      ->condition('coverage_set', $coverage_set)
      ->orderBy('path')
      ->execute()
      ->fetchAll();
    foreach ($files as &$file) {
      $file->data = json_decode($file->data, TRUE);
    }
    file_unmanaged_save_data(json_encode($files), $directory . '/coverage.json', FILE_EXISTS_REPLACE);
  }

  // Print directory in which exported data resides.
  drush_print(drupal_realpath($directory));
}

/**
 * Validate coverage set ID.
 *
 * @param $coverage_set
 *   A coverage set ID.
 *
 * @return
 *   TRUE if valid, otherwise FALSE.
 */
function drush_code_coverage_summary_validate($coverage_set = NULL) {
  if (!$coverage_set) {
    return drush_set_error('COVERAGE_SET', dt('A coverage set ID is required.'));
  }

  module_load_include('report.inc', 'code_coverage');
  if (!code_coverage_report_file_list($coverage_set)) {
    return drush_set_error('COVERAGE_SET', dt('Invalid coverage set ID.'));
  }
}

/**
 * Provide a simple code coverage summary.
 *
 * @param $coverage_set
 *   A coverage set ID.
 */
function drush_code_coverage_summary($coverage_set) {

  $query = db_select('code_coverage', 'c')
    ->condition('coverage_set', $coverage_set);

  $query->addExpression('SUM(executed)', 'executed');
  $query->addExpression('SUM(executable)', 'executable');

  $result = $query->execute()
    ->fetchAssoc();

  // Print the lines executed, executable and percentage.
  drush_print('Total coverage: ' . $result['executed'] . '/' . $result['executable'] . ' ' . number_format(($result['executed'] / $result['executable']) * 100) . '%');
}

/**
 * Validate the file.
 *
 * @param $file
 *   Path to file.
 */
function drush_code_coverage_executable_validate($file = NULL) {
  if (!$file || !file_exists($file) || !is_file($file)) {
    return drush_set_error('FILE', dt('A valid file is required.'));
  }
}

/**
 * Determine the number of executable lines contained in a file.
 *
 * Files are loaded in order to determine lines of code so this should be used
 * for files that are never executed.
 *
 * @param $path
 *   Path to file.
 */
function drush_code_coverage_executable($path) {
  module_load_include('xdebug.inc', 'code_coverage');
  module_load_include('process.inc', 'code_coverage');

  code_coverage_start();

  // Ensure that any embedded HTML or output generated by the file is hidden.
  ob_start();
  require_once $path;
  ob_end_clean();

  $coverage = xdebug_get_code_coverage();
  code_coverage_stop();

  // Check for coverage data for the specified path.
  if (isset($coverage[$realpath = realpath($path)])) {
    // Process coverage data for single file.
    $coverage = code_coverage_process_set(array($realpath => $coverage[$realpath]), FALSE);

    // If full option is present then print a full coverage description of the
    // file, otherwise print the number of executable lines.
    if (drush_get_option('full')) {
      // Since the file is assumed to be not executed ensure that each line is
      // marked as not covered.
      foreach ($coverage[$path] as $line => &$covered) {
        $covered = FALSE;
      }

      // Build an array of the data to be printed.
      $export = array(
        'path' => $path,
        'executed' => 0,
        'executable' => count($coverage[$path]),
        'data' => $coverage[$path],
      );
      drush_print(json_encode($export));
    }
    else {
      drush_print(count($coverage[$path]));
    }
  }
  else {
    return drush_set_error('NO_COVERAGE', dt('No coverage information found.'));
  }
}
