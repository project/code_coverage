Installation
-----------------

Due to the low-level entry point of code coverage the patch is hard-coded to point to sites/all/modules/code_coverage. The code_coverage module must be placed there in order to work unless you alter the patch accordingly.

In order to get code coverage working the ./code_coverage.patch will need to be applied to core.

'patch -p1 < sites/all/modules/code_coverage/code_coverage.patch'

Usage
-------------

After running tests though the UI, a code coverage link will be available for those tests.

The code coverage for an individual page can be viewed by appending ?code_coverage=true to url and clicking the link at the bottom of the page (may need to scroll).

Filter settings may be set to limit the extent of what is recorded during test runs and page runs at admin/config/development/code_coverage.

Command Line / Drush
---------------

When you run tests with the code coverage module enabled, it will generate results files and store them in the temp directory you have configured in Drupal.
If you're reading them through the command line, you will have to process the results manually. You need to pass the test id through,
which you can see if you do an 'ls' on your temp directory and look for the code_coverage_*.json files, the first digit will be the test id.

If you're trying to automate this, just know that your first test on clean setup will be 1 and you can go from there.

 'drush cc-process testid'

 ex: 'drush cc-process 1'

Once the results are processed, you can either output a coverage summary

 'drush cc-summary testid'

 or you can export all the results

 'drush cc-export testid'
 'drush cc-export testid --html'

If you want to set any filters without the gui, you will need to set the variables manually via drush before you run 'cc-process'

  'php -r "print json_encode(array('module_name'));" | drush vset --format=json code_coverage_filter_modules -'

  or

  'php -r "print json_encode(array('filename'));" | drush vset --format=json code_coverage_filter_files -'

Known Issues
-------------------

 - xdebug and php7 segfaults on 2.4.0 when attempting code coverage, you will need to upgrade to 2.4.1
 - xdebug 2.5.0 can segfault when running code coverage, use either 2.4.1 or 2.6-dev
